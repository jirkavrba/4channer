import {
    createBot,
    sendMessage,
    startBot,
} from "https://deno.land/x/discordeno@18.0.1/mod.ts";
import { load } from "https://deno.land/std@0.190.0/dotenv/mod.ts";
import { Bot } from "https://deno.land/x/discordeno@18.0.1/bot.ts";

// @deno-types="npm:@types/turndown"
import TurndownService from "npm:turndown";
import { countryCodeEmoji } from "npm:country-code-emoji";

type Config = {
    BOARDS: string;
    DISCORD_TOKEN: string;
    DISCORD_CHANNEL_ID: string;
    MINUTES_BETWEEN_POSTS: string;
};

type Thread = {
    no: number;
    sticky: 1 | 0;
    closed: 1 | 0;
    now: string;
    name: string;
    sub: string;
    com: string;
    filename: string;
    ext: string;
    w: number;
    h: number;
    tn_w: number;
    tn_h: number;
    tim: number;
    time: number;
    md5: number;
    fsize: number;
    resto: 0 | 1;
    id: string;
    country: string;
    m_img: number;
    semantic_url: string;
    country_name: string;
    replies: number;
    images: number;
    last_modified: number;
};

type Page = {
    page: number;
    threads: Array<Thread>;
};

type Catalog = Array<Page>;

await load({ examplePath: ".env.example", export: true }) as Config;

const service = new TurndownService();

const postRandomShit = (bot: Bot) => {
    return async () => {
        const board = Deno.env.get("BOARDS")?.split(",")?.sort(() => 0.5 - Math.random())?.at(0) ?? "b";
        const catalog: Catalog = await fetch(`https://a.4cdn.org/${board}/catalog.json`).then((response) => response.json());
        const thread: Thread = catalog.map((page) => page.threads)
            .flat()
            .filter(thread => thread.sticky !== 1)
            .sort(() => 0.5 - Math.random())[0];

        const flag = thread.country ? countryCodeEmoji(thread.country) + " " : "";
        const id = thread.id ? `(ID: ${thread.id})` : "";

        await sendMessage(bot, Deno.env.get("DISCORD_CHANNEL_ID")!, {
            embeds: [
                {
                    title: thread.sub ? service.turndown(thread.sub) : undefined,
                    description: thread.com ? service.turndown(thread.com) : undefined,
                    author: {
                        name: `${thread.name} ${id} ${flag}`,
                        url: `https://boards.4chan.org/${board}/thread/${thread.no}`
                    },
                    footer: {
                        text: `Thread #${thread.no} on /${board}/`
                    },
                    image: {
                        url: `https://i.4cdn.org/${board}/${thread.tim}${thread.ext}`,
                    },
                },
            ],
        });
    };
};

const bot = createBot({
    token: Deno.env.get("DISCORD_TOKEN")!,
    events: {
        ready(_bot, _payload, _raw) {
            const callback = postRandomShit(bot);

            setInterval(
                () => callback(),
                Number(Deno.env.get("MINUTES_BETWEEN_POSTS")) * 60 * 1000,
            );
            callback();

            console.log("Logged in");
        },
    },
});

await startBot(bot);
