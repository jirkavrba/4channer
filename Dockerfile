FROM denoland/deno:1.34.1

RUN mkdir /workspace
WORKDIR /workspace

COPY ./.env.example /workspace/.env.example
COPY ./index.ts /workspace/index.ts

RUN deno cache /workspace/index.ts

ENTRYPOINT [ "deno", "run", "--allow-read", "--allow-net", "--allow-env", "/workspace/index.ts" ]